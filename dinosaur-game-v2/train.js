class Train{
  constructor(){
    this.r = 75;
    this.x = width;
    this.y = height - this.r
  }
  
  move(steps){
    if(steps == null)
      steps = 16;
    this.x -= steps;
  }
  
  show(){
    image(tImg, this.x, this.y - 70, this.r, this.r)
    fill(255, 50);
    //ellipseMode(CORNER)
    //ellipse(this.x, this.y - 70, this.r, this.r)

  }
  
  offscreen(){
    return this.x < -25;
  }
  
}