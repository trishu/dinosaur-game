class Unicorn{
  constructor(){
    this.r = 100;
    this.x = 50;
    this.y = height - this.r;
    this.vy = 0;
    this.gravity = 2.5;

  }
  
  jump(sound){
    if(this.y == height - this.r){
      this.vy = -30;
      sound.play() 
    }
  }
  
  hits(train){
    let x1 = this.x + this.r * 0.5;
    let y1 = (this.y)  + this.r * 0.5;
    let x2 = train.x + train.r * 0.5;
    let y2 = (train.y) + train.r * 0.5;

    let r = collideCircleCircle(x1, y1, this.r, 
                   x2, y2, train.r)

    if(r == true){
      this.r = 100;
      this.x = 50;
      this.y = height - this.r;
      this.vy = 0;
      this.gravity = 2.5;
      //console.log("r si true  " + r);
      this.y += 170;
      train.y += 170;
      train.show();
      this.show();
    }

    return r;
    //return collideRectRect(this.x, this.y, this.r, this.r, 
      //             train.x, train.y, train.r, train.r)
  }
  
  move(){
    this.y += this.vy;
    this.vy += this.gravity;
    this.y = constrain(this.y, 0, height - this.r);
  }
  show(){ 
    image(uImg, this.x, this.y - 70, this.r, this.r)
    fill(255, 50);
    //ellipseMode(CORNER)
    //ellipse(this.x, this.y - 70, this.r, this.r)
  }
}