let unicorn;
let uImg;
let tImg;
let bImg;
let trains = []
let bgoffset = 0
let speed = 0;
const maxSpeed = 0.2
let score = 0;
let offsetX = 0;
let gameOver = false;
let trainSpeed = 0;

const spacing = 80;
let timer = spacing;
let r = 1;


function preload(){
  uImg = loadImage('dino4.png');
  tImg = loadImage('cactus.png');
  bImg = loadImage('back.jpg');
  
  
  gameOverAudio = loadSound('game-over-sound.mp3');
  jumpAudio = loadSound('jump-sound.mp3');

}

//function mousePressed(){
//  trains.push(new Train());
//}

function setup() {
  createCanvas(800, 450);
  unicorn = new Unicorn();
  
}


function keyPressed(){
  if(key == ' '){
    //jumpAudio.play()
    unicorn.jump(jumpAudio);
  }
  else{
    if(gameOver == true){
        unicorn.show();
        loop();
    }
  }
}



function draw() {
  //if(random(1) < 0.005){
  //  trains.push(new Train());
  //}

  timer--;
  if (timer <= 0) {
    timer = 0;
    r = random(1);
    if (r < 0.015) {
      trains.push(new Train());
      timer = spacing;
    }
  }


      

  
  score = score + (0.5*speed) + 0.1;
  speed += (score.toFixed(1) % 10 == 0) ? 0.001 : 0;
  speed = constrain(speed, 0, maxSpeed);
  
  //console.log(score, speed)
  bgoffset = (bgoffset + map(speed, 0, maxSpeed, 10, 20)) % width;
  //background(bImg, -bgoffset, 380);
  //background(bImg, -bgoffset + width, 380);
  background(0);
  //fill(0, 255, 0);
  image(bImg, offsetX, 0, width, height);
	
  //fill(0, 0, 255);
  image(bImg,offsetX + width, 0, width, height);
	
  offsetX -= 5;
  if(offsetX <= -width){
 	offsetX = 0;	
  }  
  
  fill(245, 220, 35);
  textSize(25);
  stroke(200, 158, 77);
  text(`Cactus is coming after ${nf(timer/frameRate(), 1, 2)}`, 10 , 25);
  
  textSize(25);
  fill(245, 220, 35);
  stroke(200, 158, 77);
  text("Score", width - 100, 25);
  text(parseInt(score), width - 100, 50)
  
  
  for(let t of trains){
    trainSpeed = map(speed, 0, maxSpeed, 12, 20)
    t.move(trainSpeed);
    t.show();
    if(unicorn.hits(t)){
      console.log('game over');
      background(0, 100)
      
      unicorn.show();
      gameOverAudio.play();
      textFont("default");
      fill(245, 220, 35);
      stroke(255, 158, 77);
      textSize(25);
      text("GAME OVER !!!", width / 2.4, height / 2.5);
      
      
      fill(255);
      noStroke();
      textSize(18);
      text("Press any key to restart...", width / 2.8, height / 1.6);
      
      
      fill(0, 255, 211);
      textSize(25);
      text("New Highscore is " + parseInt(score) + " !!!", width / 2.53, 60);
      
      score = 0;
      speed = 0;
      gameOver = true;
      noLoop()
    }
  }
  
  unicorn.show();
  unicorn.move();

}